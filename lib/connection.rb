require 'faraday'

class Connection
  BASE_URL = 'https://pro-api.coinmarketcap.com/v1/'

  def self.set
    Faraday.new(url: BASE_URL) do |http|
      http.headers['Content-Type'] = 'application/json'
      http.headers['X-CMC_PRO_API_KEY'] = ENV['COINMARKETCAP_KEY']
      http.adapter Faraday.default_adapter
    end
  end
end
