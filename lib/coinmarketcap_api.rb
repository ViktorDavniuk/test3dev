require 'connection'
require 'coinmarketcap_api_exceptions'
require 'json'

class CoinmarketcapApi
  class << self

    # Sends a GET request to coinmarketcap API and returns currencies as a hash or an error.
    # shift - Optionally offset the start (1-based index) of the paginated list of items to return.
    # limit - Optionally specify the number of results to return.
    def get_currencies(shift=1, limit=100)
      response = safe_request(:get, 'cryptocurrency/listings/latest', {
        start: shift,
        limit: limit,
        sort: "date_added",
        sort_dir: "asc"
      })
      cur = JSON.parse(response.body)
      cur["data"]
    end

    private

    # realises retry with exponential backoff patern
    # retries requests with increasing pause interval if got errors
    # *args are [{faraday instance method (symbol)}, {argument 1}, {argument 2} e. t. c]
    def safe_request(*args)
      tries = 0
      loop do
        response = api.public_send(*args)
        if ([429, 501].include?(response.status) && tries < 10)
          tries += 1
          sleep 2*tries
        else
          break;
        end
      end
      return response if response_successful?(response)
      raise error_class(response), {status: "error", code: response[:error_code], message: response[:error_message]}
    end

    def error_class(response)
      case response.status
        when 400
          CoinmarketcapApiExceptions::BadRequestError
        when 401
          CoinmarketcapApiExceptions::UnauthorizedError
        when 403
          CoinmarketcapApiExceptions::ForbiddenError
        when 429
          CoinmarketcapApiExceptions::TooManyRequests
        when 500
          CoinmarketcapApiExceptions::InternalServerError
        else
          CoinmarketcapApiExceptions::ClientAccessError
      end
    end

    def response_successful?(response)
      response.status == 200
    end

    def api
      Connection.set
    end
  end
end
