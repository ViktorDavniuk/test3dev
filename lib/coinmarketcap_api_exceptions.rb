module CoinmarketcapApiExceptions
  class BaseException < StandardError; end

  class BadRequestError < CoinmarketcapApiExceptions::BaseException; end

  class UnauthorizedError < CoinmarketcapApiExceptions::BaseException; end

  class ForbiddenError < CoinmarketcapApiExceptions::BaseException; end

  class TooManyRequests < CoinmarketcapApiExceptions::BaseException; end

  class InternalServerError < CoinmarketcapApiExceptions::BaseException; end

  class ClientAccessError < CoinmarketcapApiExceptions::BaseException; end

  class ClientAccessError < CoinmarketcapApiExceptions::BaseException; end
end
