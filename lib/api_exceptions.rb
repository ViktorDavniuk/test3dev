module ApiExceptions
  class BaseException < StandardError
    include ActiveModel::Serialization
    attr_reader :status, :code, :message

    ERROR_DESCRIPTION = Proc.new {|code, message| {status: "error", code: code, message: message}}

    ERROR_CODE_MAP = {
      "ConversionError::MissingBaseCurrency" =>
        ERROR_DESCRIPTION.call(4001, "Can't convert without a base currency"),
      "ConversionError::MissingConvertCurrency" =>
        ERROR_DESCRIPTION.call(4002, "Can't convert without a destination currency"),
      "ConversionError::MissingBaseAmount" =>
        ERROR_DESCRIPTION.call(4003, "Base amount is not exist"),
      "ConversionError::MissingConvertAmount" =>
        ERROR_DESCRIPTION.call(4004, "Converted amount is not exist"),
      "ConversionError::BaseCurrencyNotFound" =>
        ERROR_DESCRIPTION.call(4005, "Can't find base currency"),
      "ConversionError::ConvertCurrencyNotFound" =>
        ERROR_DESCRIPTION.call(4006, "Can't find convert currency"),
    }

    def initialize
      error_type = self.class.name.scan(/ApiExceptions::(.*)/).flatten.first
      ApiExceptions::BaseException::ERROR_CODE_MAP
        .fetch(error_type, {}).each do |attr, value|
          instance_variable_set("@#{attr}".to_sym, value)
      end
    end
  end

  class ConversionError < ApiExceptions::BaseException; end

  class MissingBaseCurrency < ApiExceptions::ConversionError; end

  class MissingConvertCurrency < ApiExceptions::ConversionError; end

  class MissingBaseAmount < ApiExceptions::ConversionError; end

  class MissingConvertAmount < ApiExceptions::ConversionError; end

  class BaseCurrencyNotFound < ApiExceptions::ConversionError; end

  class ConvertCurrencyNotFound < ApiExceptions::ConversionError; end
end
