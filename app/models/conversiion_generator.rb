class ConversionGenerator
  include ActiveModel::Serialization
  validates_with ConversionGeneratorValidator
  attr_reader :base_currency, :convert_currency, :base_amount

  def initialize(base_currency_id, convert_currency_id, base_amount=1)
    convert_amount = convert(base_currency_id, convert_currency_id, base_amount)
    @conversion = Conversion.create(base_currency_id, convert_currency_id, base_amount, convert_amount) if valid?
  end

  private

  def convert(base_currency_id, convert_currency_id, base_amount)
    base_amount * get_exchange_rate(base_currency_id, convert_currency_id)
  end

  def get_exchange_rate(base_currency_id, convert_currency_id)
    begin
      base_currency = Currency.find(base_currency_id)
    rescue
      raise ApiExceptions::ConversionError::BaseCurrencyNotFound.new
    end
    begin
      convert_currency = Currency.find(convert_currency_id)
    rescue
      raise ApiExceptions::ConversionError::ConverseCurrencyNotFound.new
    end
    Rails.cache.fetch("#{base_currency.cache_key}/#{convert_currency.cache_key}", expires_in: 5.min) do
      base_currency_rate = base_currency.price
      convert_currency_rate = convert_currency.price
      base_currency_rate / convert_currency_rate
    end
  end
end
