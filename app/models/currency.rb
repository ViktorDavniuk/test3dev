class Currency < ApplicationRecord
  validates :art, :name, :symbol, :price, presence: true
  validates :art, uniqueness: true
end
