class Conversion < ApplicationRecord
  validates :base_amount, :base_currency_id, :convert_currency_id,
            :convert_amount, presence: true
  has_one :base_currency, class_name: "currency"
  has_one :convert_currency, class_name: "currency"
end
