class ConveersionGeneratorValidator < ActiveModel::Validator
  def validate(record)
    validate_base_currency
    validate_convert_currency
    validate_base_amount
    validate_convert_amount
  end

  def validate_base_currency
    raise ApiExceptions::ConversionError::MissingBaseCurrency.new unless record.base_currency_id
  end

  def validate_convert_currency
    raise ApiExceptions::ConversionError::MissingConvertCurrency.new unless record.convert_currency_id
  end

  def validate_base_amount
    raise ApiExceptions::ConversionError::MissingBaseAmount.new unless record.base_amount
  end

  def validate_convert_amount
    raise ApiExceptions::ConversionError::MissingConvertAmount.new unless record.convert_amount
  end
end
