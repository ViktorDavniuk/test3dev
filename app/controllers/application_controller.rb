class ApplicationController < ActionController::API

  rescue_from ApiExceptions::BaseException,
    :with => :render_api_response_error

  def render_api_response_error(error)
    render json: error, serializer: ApiExceptionsSerializer, status: 200
  end
end
