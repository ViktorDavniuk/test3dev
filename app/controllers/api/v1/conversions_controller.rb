class Api::V1::ConversionsController < ApplicationController

  def last
    conversions = Conversion.last(params['count'] || 10)
    render json: conversions, status: 200
  end

  def convert
    conversion_generator = ConversionGenerator.new(
      base_currency_id: params[:base_currency_id],
      convert_currency_id: params[:convert_currency_id],
      base_amount: params[:base_amount])
    render json: conversion_generator
  end

  private

  def permitted_params
    params.require(:conversion).permit(:base_currency_id, :convert_currency_id, :base_amount)
  end
end
