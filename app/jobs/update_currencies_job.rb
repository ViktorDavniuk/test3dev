require 'coinmarketcap_api'

class UpdateCurrenciesJob
  include Sidekiq::Worker

  # gets currencies info from the API bulk by bulk and goes through it with createing or updating
  def perform
    shift = 1
    loop do
        currencies = CoinmarketcapApi.get_currencies(shift)
        create_or_update(currencies)
        shift += 100
        break if currencies.count == 0
    end
  end

  private

  # updates the currency info if it exsists or creates a new
  # currencies - hash of currencies to create or update
  def create_or_update(currencies)
    currencies.each do |cur|
      record = Currency.find_or_create_by!(art: cur["id"]) do |perf_cur|
        perf_cur.assign_attributes(format_currency_attributes(cur))
      end
      puts record
    end
  end

  # format currency to one-level structure and
  # saves id attribute from coinmarketcap to unique art attribute for simplicity and escape collisions
  def format_currency_attributes(cur)
    { art: cur["id"],
      name: cur["name"],
      symbol: cur["symbol"],
      price: cur["quote"]["USD"]["price"] }
  end
end
