Rails.application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :currencies, only: [:index]
      resources :conversions, only: [] do
        collection do
          get 'last'
          get '/', action: 'convert'
        end
      end
    end
  end
end
