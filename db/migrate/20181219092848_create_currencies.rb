class CreateCurrencies < ActiveRecord::Migration[5.2]
  def change
    create_table :currencies do |t|
      t.integer :art, unique: true, null: false
      t.string :name
      t.string :symbol
      t.float :price

      t.timestamps
    end
  end
end
