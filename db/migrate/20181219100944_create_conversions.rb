class CreateConversions < ActiveRecord::Migration[5.2]
  def change
    create_table :conversions do |t|
      t.float :base_amount, null: false
      t.integer :base_currency_id, null: false
      t.float :convert_currency_id, null: false
      t.float :convert_amount, null: false

      t.timestamps
    end
  end
end
